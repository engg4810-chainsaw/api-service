Tracker README
==================

The Tracker module provides an API web service for the portable logging
device project.

Written in Python, the module uses Pyramid as the base web framework. The
web service offers an API to access files stored in the cloud storage
location, including uploading new files for storage.

As this module is designed to be standalone, Cross Origin Requests (CORS)
are allowed on all views.


Getting Started (Development)
-----------------------------

It is recommended that development be conducted within a virtual environment,
to ensure no conflicts between packages.

- cd <directory containing this file>

- $venv/bin/python setup.py develop

- $venv/bin/initialize_Test_db development.ini

- $venv/bin/pserve development.ini


Deployment
----------

To deploy the Tracker API, this module and its dependencies (found in
requirements.txt) need to be installed on the target server. To complete
installation, consult the manual for your preferred web server to determine
how to enable Python WSGI or similar.
