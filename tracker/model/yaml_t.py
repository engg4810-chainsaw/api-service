import datetime
import yaml
try:
    from yaml import CLoader as Loader, CDumper as Dumper
except ImportError:
    from yaml import Loader, Dumper


def represent_datetime_iso8601(dumper, data):
    value = unicode(data.isoformat())
    return dumper.represent_scalar(u'tag:yaml.org,2002:timestamp', value)


yaml.add_representer(
    datetime.datetime, represent_datetime_iso8601, Dumper=Dumper)


def dump(data, stream=None, Dumper=Dumper, **kw):
    # We do not want tags output, so do not use default flow style
    kw.setdefault('default_flow_style', False)
    kw.setdefault('explicit_end', False)
    return yaml.dump(data, stream, Dumper=Dumper, **kw)


def load(stream, Loader=Loader):
    return yaml.load(stream, Loader)
