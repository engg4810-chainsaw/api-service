from inspect import getmro
import json
import datetime
import pytz


def encode_datetime(obj):
    """
    Encode datetime as ISO-8601 string.
    See http://en.wikipedia.org/wiki/ISO_8601

    If the datetime is timezone-naive then UTC is assumed.
    """
    if obj.tzinfo is None:
        obj = obj.replace(tzinfo=pytz.UTC)
    return obj.isoformat()


class ExtendedJSONEncoder(json.JSONEncoder):

    extra_encoders = {
        datetime.date: encode_datetime,
        datetime.datetime: encode_datetime,
    }

    def default(self, obj):
        try:
            cls = obj.__class__
        except AttributeError:
            cls = type(obj)
        for type_ in getmro(cls):
            encoder = self.extra_encoders.get(type_, None)
            if encoder is not None:
                return encoder(obj)
        return json.JSONEncoder.default(self, obj)

_encoder = ExtendedJSONEncoder(
    skipkeys=False, ensure_ascii=True,
    check_circular=True, allow_nan=False, indent=None,
    separators=None, encoding='utf-8',
    sort_keys=False,
    )


def dump_json(obj):
    """Encode an object using the extended JSON encoder."""
    return _encoder.encode(obj)
