from __future__ import division
import os
import fnmatch
import io
import struct
import datetime
import paramiko
import sftpsync
import pytz

from yaml_t import dump, load

# Default locations for private keys
DEFAULT_RSA_KEY = '~/.ssh/id_rsa'
DEFAULT_DSA_KEY = '~/.ssh/id_dsa'

# Data format from logging device
RAW_FORMAT = "=BBHBBBBBiihhhhHI"
RAW_FORMAT_SIZE = struct.calcsize(RAW_FORMAT)

# Constants for unpacking data
VALID_TIME = 7  # Valid UTC time, week number and time of week
GPS_FACTOR = 1e-7
ACC_FACTOR = 16 / 4096
PRESSURE_FACTOR = 34337
TEMP_FACTOR = 10

# Data format for logging device tracking mode
CONFIG_FORMAT = "=iiB"
CONFIG_FORMAT_SIZE = struct.calcsize(CONFIG_FORMAT)


def list_dirs(directory):
    """
    Lists all subdirectories of the specified directory.
    """
    try:
        contents = os.walk(directory).next()
        dirs = contents[1]
    except StopIteration:
        # The directory was not found.
        return None
    return dirs


def list_files(directory, file_extentions=None):
    """
    List all files in the specified directory.

    If `file_extentions` is provided, filter by the given file extension.
    """
    matches = []
    try:
        contents = os.walk(directory).next()
        files = contents[2]
        if file_extentions is not None:
            for ext in file_extentions:
                matches.extend(fnmatch.filter(files, ext))
        else:
            matches = files
    except StopIteration:
        # The directory was not found.
        return None
    return matches


def list_directory(
        directory, filter_include=None, dirs_only=False, relative=True):
    """
    Recursively list all files and directories in the specified directory.

    If `filter_include` is provided, filter by the given file extension.
    """
    tree = []

    directory = directory.rstrip(os.sep)
    start = directory.rfind(os.sep) + 1

    for path, dirs, files in os.walk(directory):
        folders = path[start:].split(os.sep)

        if relative:
            # Make paths relative to the root directory
            path = path.replace(directory, '')
            if path.startswith(os.sep):
                path = path.replace(os.sep, '', 1)

        # Create item for current path
        item = {'path': path, 'name': folders[-1], 'children': []}

        if not dirs_only:
            # Filter the files
            matches = files
            if filter_include is not None:
                matches = []
                for ext in filter_include:
                    matches.extend(fnmatch.filter(files, ext))

            # Add files that are direct children
            for f in matches:
                p = os.path.join(path, f)
                item['children'].append({
                    'path': p,
                    'name': f.replace('\\', '\\\\'),  # Escape backslashes
                    'children': []
                })

        # Find the parent to store current path
        parent = reduce(_find_parent, folders[:-1], tree)
        parent.append(item)

    return tree


def _find_parent(items, path):
    for item in items:
        if item['name'] == path:
            return item['children']
    return items


def open_data_file(path):
    """
    Open the given file and convert it from the online storage format (YAML)
    to a Python dictionary.
    """
    contents = None
    try:
        with open(path, 'rU') as f:
            contents = load(f)
    except:
        pass

    # Invalid if it is not a dict containing 'samples' and 'metadata' keys.
    if not isinstance(contents, dict) or (
            not contents.get('samples', False) or
            not contents.get('metadata', False)):
        contents = None

    return contents


def group_dir_path(groups_dir, *args):
    """ Returns the path to the directory for group. """
    return os.path.join(groups_dir, *args)


def process_data(data_file):
    """
    Process data from an open raw data file (containing binary data),
    converting it to a list of data points.
    """

    data = []

    while True:
        # Read one section at a time
        line = data_file.read(RAW_FORMAT_SIZE)
        if line == '':
            break

        # Extract to numbers
        raw = struct.unpack(RAW_FORMAT, line)

        reading = {
            'fixes': raw[0],
            'timeValid': raw[1],
            'year': raw[2],
            'month': raw[3],
            'day': raw[4],
            'hour': raw[5],
            'min': raw[6],
            'sec': raw[7],
            'long': raw[8] * GPS_FACTOR,
            'lat': raw[9] * GPS_FACTOR,
            'acc': {
                'x': raw[10] * ACC_FACTOR,
                'y': raw[11] * ACC_FACTOR,
                'z': raw[12] * ACC_FACTOR
            },
            'temp': raw[13] / TEMP_FACTOR,
            'lux': raw[14],
            'pressure': (raw[15] - PRESSURE_FACTOR) / 1000,
        }
        data.append(reading)

    return data


def create_data_file(team, config, comment, samples, **debug):
    """Formats data according to the required data format."""
    processed_samples = []

    for item in samples:
        acc = [item['acc']['x'], item['acc']['y'], item['acc']['z']]

        sample = {
            'temperature': item['temp'],
            'acceleration': acc,
            'pressure': item['pressure'],
            # 'sound': item['sound'],
            # 'humidity': item['humidity'],
            # 'magnetic_field': [25, 45e0, 15],
            'luminosity': item['lux'],
            # 'uv': item['uv'],
        }

        if item['timeValid'] == VALID_TIME:
            time = datetime.datetime(
                item['year'], item['month'], item['day'],
                item['hour'], item['min'], item['sec'], tzinfo=pytz.utc)
            sample['time'] = time

        if item['fixes'] > 2:
            # Include the GPS coordinates if they were recorded
            sample['latitude'] = item['lat']
            sample['longitude'] = item['long']

        processed_samples.append(sample)

    return {
        'metadata': {
            'team_number': int(team),
            'configuration_name': config,
            'comment': comment,
        },
        'samples': processed_samples,
        'debug': debug
    }


def output_yaml_file(filename, data):
    """ Output data to a file in YAML format """
    with open(filename, 'w') as f:
        dump(data, f)


def create_config(points):
    f = io.BytesIO()

    for point in points:
        lat = point['lat'] / GPS_FACTOR
        lng = point['lng'] / GPS_FACTOR
        data = struct.pack(CONFIG_FORMAT, lat, lng, point['high'])
        f.write(data)

    return f


def load_directory_contents(directory, filter_include=None):
    """
    Recursively search the directory for data files, and merge them into
    a single structure.
    """
    combined = {'samples': []}

    for path, dirs, files in os.walk(directory):
        # Filter the files
        matches = files
        if filter_include is not None:
            matches = []
            for ext in filter_include:
                matches.extend(fnmatch.filter(files, ext))

        for f in matches:
            # Load the file
            fp = os.path.join(path, f)
            data = open_data_file(fp)
            if data is not None:
                combined['samples'].extend(data['samples'])

    return combined


class RemoteSync(object):
    def __init__(self, host, username, key_file):
        self.host = host
        self.username = username
        self.key_file = key_file
        self.key = None
        self.sftp = None

    def open_sftp(self):
        if self.sftp:
            return
        self.sftp = sftpsync.SftpSync(
            self.host, self.username, pkey=self.remote_key())

    def remote_list_dir(self, remote_dir):
        self.open_sftp()
        return self.sftp.sftp.listdir(remote_dir)

    def sync_remote_local(self, remote_dir, local_dir, file_extentions=None):
        self.open_sftp()
        self.sftp.sync(
            remote_dir, local_dir, delete=True, include=file_extentions)

    def sync_local_remote(self, local_dir, remote_dir, file_extentions=None):
        self.open_sftp()
        self.sftp.sync(
            local_dir, remote_dir, download=False, include=file_extentions)

    def sync_folder(self, remote_dir, local_dir, file_extentions=None):
        # List remote dir
        remote_files = self.conn.sftp.listdir(remote_dir)
        if file_extentions is not None:
            matches = []
            for ext in file_extentions:
                matches.extend(fnmatch.filter(remote_files, ext))
            remote_files = matches

        # List local dir
        local_files = list_files(local_dir, file_extentions)

        # Delete local files not on remote
        delete = set(local_files) - set(remote_files)
        for filename in delete:
            # Delete the file locally, as it was deleted on the remote server.
            os.remove(os.path.join(local_dir, filename))

        # If remote file is newer than local, copy to local
        pass

    def remote_key(self):
        if self.key:
            return self.key

        private_key = self.key_file
        if not private_key:
            # Try to use default key.
            if os.path.exists(os.path.expanduser(DEFAULT_RSA_KEY)):
                private_key = DEFAULT_RSA_KEY
            elif os.path.exists(os.path.expanduser(DEFAULT_DSA_KEY)):
                private_key = DEFAULT_DSA_KEY
            else:
                raise TypeError("You have not specified a password or key.")

        private_key_file = os.path.expanduser(private_key)
        try:
            # First try rsa
            self.key = paramiko.RSAKey.from_private_key_file(private_key_file)
        except paramiko.SSHException:
            # If it fails, try dss/dsa
            self.key = paramiko.DSSKey.from_private_key_file(private_key_file)

        return self.key

    def perform_sync(self, remote, local, storage, file_extensions=None):
        """Perform synchronisation via SFTP from the remote server to a local
        destination. Sync is one direction (remote -> local), except for
        the storage dir, which is synchronised in the opposite direction.
        """
        remote_dirs = self.remote_list_dir(remote)

        # First, sync the storage folders.
        for d in storage:
            local_path = os.path.join(local, d)
            remote_path = os.path.join(remote, d)
            self.sync_local_remote(local_path, remote_path, file_extensions)

        # Now sync all of the other folders.
        for d in remote_dirs:
            local_path = os.path.join(local, d)
            remote_path = os.path.join(remote, d)
            self.sync_remote_local(remote_path, local_path, file_extensions)
