import os
from pyramid.config import Configurator
from pyramid.events import NewRequest
from sqlalchemy import engine_from_config

from paste.deploy.converters import asbool, asint, aslist

from .models import (
    DBSession,
    Base,
    )
from model.api import RemoteSync
from model.json_encode import dump_json


def main(global_config, **settings):
    """ This function returns a Pyramid WSGI application. """
    # Ensure the Tracker groups directory exists
    groups_dir = settings['tracker.groups_dir']
    if not os.path.exists(groups_dir):
        raise RuntimeError(
            "The groups directory '{0}' does not exist".format(groups_dir))

    # Ensure all INI settings are in corresponding Python types.
    settings['tracker.file_types'] = aslist(settings['tracker.file_types'])
    settings['tracker.storage_dirs'] = aslist(settings['tracker.storage_dirs'])
    sync_enabled = asbool(settings['tracker.sync_enabled'])
    settings['tracker.sync_enabled'] = sync_enabled
    interval = asint(settings['tracker.sync_interval'])
    settings['tracker.sync_interval'] = interval

    # Configure temporary storage
    settings['tracker.temp'] = {}

    # Create the config
    config = Configurator(settings=settings)

    engine = engine_from_config(settings, 'sqlalchemy.')
    DBSession.configure(bind=engine)
    Base.metadata.bind = engine

    config.add_static_view('static', 'static', cache_max_age=3600)

    # Add routes.
    config.add_route('index', '/')
    config.add_route('list_groups', '/groups')
    config.add_route('list_files', '/files/*path')
    config.add_route('dir_tree', '/dir_tree*path')
    config.add_route('dir_only_tree', '/dir_only_tree*path')
    config.add_route('data', '/data/{group}/*path')
    config.add_route('storage_groups', '/storage_groups')
    config.add_route('upload', '/upload', request_method='POST')
    config.add_route('config', '/config', request_method='POST')
    config.add_route('download_track_config', '/download_config/{filename}')
    config.add_route('download_wom_config', '/download_wom_config')
    config.add_route('multiple_data', '/dir_data/{group}*path')

    # Add CORS handling
    config.add_subscriber(add_cors_headers_response_callback, NewRequest)
    config.add_route('options', '/*path', request_method='OPTIONS')

    # Add custom JSON renderer
    config.add_renderer('json', 'tracker.json_renderer_factory')

    # Scan files.
    config.scan()

    # Setup the synchronisation
    if settings['tracker.sync_enabled']:
        rs = RemoteSync(
            settings['tracker.server'],
            settings['tracker.user'],
            settings['tracker.private_key'])
        args = (
            rs,
            settings['tracker.groups_dir'],
            settings['tracker.remote_groups_dir'],
            settings['tracker.storage_dirs'],
            settings['tracker.file_types'],
        )

        # Schedule synchronisation as per interval.
        config.registry.scheduler.add_interval_job(
            group_synchronise_task, args=args, minutes=interval)

        # Run initial synchronisation.
        group_synchronise_task(*args)

    # Launch the web server.
    return config.make_wsgi_app()


def group_synchronise_task(rs, local_groups, remote_groups, storage, include):
    """ Synchronise the group folders on a schedule """
    rs.perform_sync(remote_groups, local_groups, storage, include)


def json_renderer_factory(info):
    def render_json(value, system):
        request = system.get('request')
        if request is not None:
            response = request.response
            if response.content_type == response.default_content_type:
                response.content_type = 'application/json'
        return dump_json(value)
    return render_json


def add_cors_headers_response_callback(event):
    def cors_headers(request, response):
        # Allow Cross-Origin Requests.
        request.response.headerlist.extend(
            (
                ('Access-Control-Allow-Origin', '*'),
                ('Access-Control-Allow-Methods',
                    'POST,GET,DELETE,PUT,OPTIONS'),
                ('Access-Control-Allow-Headers',
                    'Origin, Content-Type, Accept, Authorization'),
                ('Access-Control-Allow-Credentials', 'true'),
                ('Access-Control-Max-Age', '1728000'),
            )
        )
    event.request.add_response_callback(cors_headers)
