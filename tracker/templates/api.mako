<!DOCTYPE html>
<html>
<head>
  <title>Tracker API</title>
  <meta http-equiv="Content-Type" content="text/html;charset=UTF-8"/>
  <link rel="shortcut icon" href="${request.static_url('tracker:static/favicon.ico')}" />
</head>
<body>
  <header>
    <h1>Tracker API</h1>
  </header>
  <div>
    <p>The Tracker API provides access to data stored in the cloud.</p>
  </div>
  <footer>
    <div class="footer">&copy; Copyright 2014, Joel Addison.</div>
  </footer>
</body>
</html>
