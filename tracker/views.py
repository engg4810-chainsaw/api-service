from pyramid.view import view_config
import uuid
from model import api


# Route to handle OPTIONS requests
@view_config(route_name='options', request_method='OPTIONS')
def options(request):
    return request.response


@view_config(route_name='index', renderer='templates/api.mako')
def api_index(request):
    return {'project': 'Tracker'}


@view_config(route_name='list_groups', renderer='json')
def list_groups(request):
    groups_dir = request.registry.settings['tracker.groups_dir']
    groups = api.list_dirs(groups_dir)
    if groups is None:
        return {'error': 'No group directories were found'}
    return {'groups': groups}


@view_config(route_name='list_files', renderer='json')
def list_files(request):
    groups_dir = request.registry.settings['tracker.groups_dir']
    file_types = request.registry.settings['tracker.file_types']

    path_parts = map(str, request.matchdict['path'])
    dir_path = api.group_dir_path(groups_dir, *path_parts)

    files = api.list_files(dir_path, file_types)

    if files is None:
        path = api.group_dir_path(*path_parts)
        return {'error': 'The folder {0} could not be found'.format(path)}

    return {'files': files}


@view_config(route_name='dir_tree', renderer='json')
def directory_tree(request):
    groups_dir = request.registry.settings['tracker.groups_dir']
    file_types = request.registry.settings['tracker.file_types']

    path_parts = map(str, request.matchdict['path'])
    dir_path = api.group_dir_path(groups_dir, *path_parts)

    return api.list_directory(dir_path, file_types)


@view_config(route_name='dir_only_tree', renderer='json')
def dir_only_tree(request):
    groups_dir = request.registry.settings['tracker.groups_dir']
    file_types = request.registry.settings['tracker.file_types']

    path_parts = map(str, request.matchdict['path'])
    dir_path = api.group_dir_path(groups_dir, *path_parts)

    return api.list_directory(dir_path, file_types, dirs_only=True)


@view_config(route_name='data', renderer='json')
def load_data(request):
    group = request.matchdict['group']
    path = request.matchdict['path']

    groups_dir = request.registry.settings['tracker.groups_dir']
    file_path = api.group_dir_path(groups_dir, group, *path)

    data = api.open_data_file(file_path)

    if data is None:
        file_path = api.group_dir_path(group, *path)
        return {
            'error': 'The file {0} is not a valid data file.'.format(file_path)
        }

    return data


@view_config(route_name='storage_groups', renderer='json')
def storage_groups(request):
    groups = request.registry.settings['tracker.storage_dirs']
    if groups is None:
        return {'error': 'No storage directories are available'}
    return {'groups': groups}


@view_config(route_name='upload', renderer='json', request_method='POST')
def upload_data(request):
    team = str(request.POST['team'])
    comment = str(request.POST['comment'])
    config = str(request.POST['config'])
    group = str(request.POST['group'])
    filename = str(request.POST['filename'])
    filename = '{0}.yaml'.format(filename.strip())

    file_ = request.POST['file']
    file_data = file_.file

    groups_dir = request.registry.settings['tracker.groups_dir']
    file_path = api.group_dir_path(groups_dir, group, filename)

    # Process the raw data into YAML
    samples = api.process_data(file_data)

    data = api.create_data_file(team, config, comment, samples)

    # Write the file
    api.output_yaml_file(file_path, data)


@view_config(route_name='config', renderer='json')
def config_file(request):
    try:
        points = request.POST['points']
    except KeyError:
        points = request.json_body['points']

    fid = uuid.uuid4().get_hex()
    f = api.create_config(points)

    # Store for download
    request.registry.settings['tracker.temp'][fid] = f.getvalue()

    return {'fid': fid}


@view_config(route_name='download_track_config', renderer='string')
def download_track_config(request):
    internal_filename = request.matchdict['filename']

    # Use the configured track filename
    filename = request.registry.settings['tracker.track_cfg']

    # Specify that the response is a file to be downloaded
    request.response.content_type = "text/plain; name=%s" % filename
    request.response.content_disposition = 'attachment; filename=%s' % filename

    # Load the file contents from the storage, then delete it
    contents = request.registry.settings['tracker.temp'][internal_filename]
    del request.registry.settings['tracker.temp'][internal_filename]
    return contents


@view_config(route_name='download_wom_config', renderer='string')
def download_wom_config(request):
    # Use the configured wake on movement filename
    filename = request.registry.settings['tracker.wake_cfg']

    # Specify that the response is a file to be downloaded
    request.response.content_type = "text/plain; name=%s" % filename
    request.response.content_disposition = 'attachment; filename=%s' % filename

    return ''


@view_config(route_name='multiple_data', renderer='json')
def load_data_directory(request):
    group = request.matchdict['group']
    path = request.matchdict['path']

    groups_dir = request.registry.settings['tracker.groups_dir']
    dir_path = api.group_dir_path(groups_dir, group, *path)

    file_types = request.registry.settings['tracker.file_types']
    data = api.load_directory_contents(dir_path, file_types)

    return data
